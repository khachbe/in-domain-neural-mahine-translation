The report explain the scope and the details of the approach followed in this research.
The logs_training show the output of the training, they include the iterations of the training until the loss of the model started to increase, which is where we make a cut in the training.
The logs_bleu_eval shows the bleu score obtained for each approach: in_domain, fine_tuned, tagged.
The scripts are based on the instructions provided in google cloud, under the "Training transformer on Cloud TPU (TF 2.x)" tutorial:
Follow the tutorial in the link to understand how to setup the local dirs, how to copy output files of the model to google storage and how to use the scripts:
https://cloud.google.com/tpu/docs/tutorials/transformer-2.x?authuser=1
 - The *.sh files execute the transformer models and stores the results in google storage (train the model, and eval the model to get the bleu scores)
 - The *.py files are modifications of the data_download.py file found in the google VM for the transformer tutorial


*The original files of the hungarian-english corpora can be found in: https://ufal.mff.cuni.cz/ufal_medical_corpus A personal license is riquired
We splitted the hungarian-english corpora as follow:

For the *.py to work, you must split the corpora as follows: 
- in_domain:
hu-en-medical-train-source.txt
hu-en-medical-train-targed.txt
hu-en-medical-dev-source.txt
hu-en-medical-dev-target.txt
hu-en-medical-test-source.txt
hu-en-medical-test-target.txt
save the files in: /tmp/in_domain_raw

- out_of_domain:
hu-en-general-train-source.txt
hu-en-general-train-targed.txt
hu-en-general-dev-source.txt
hu-en-general-dev-target.txt
hu-en-general-test-source.txt
hu-en-general-test-target.txt
save the files in: /tmp/out_of_domain_raw

- tagged:
hu-en-tagged-train-source.txt
hu-en-tagged-train-targed.txt
hu-en-tagged-dev-source.txt
hu-en-tagged-dev-target.txt
hu-en-tagged-test-source.txt
hu-en-tagged-test-target.txt
save the files in: /tmp/tagged_raw
