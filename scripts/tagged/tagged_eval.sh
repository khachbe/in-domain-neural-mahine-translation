#!/bin/bash
export PYTHONPATH="$PYTHONPATH:/usr/share/models"
export STORAGE_BUCKET=gs://{name of google storage bucket}
export GCS_DATA_DIR=$STORAGE_BUCKET/data/ufal_tagged
export MODEL_DIR=$STORAGE_BUCKET/transformer/model_tagged
export TPU_NAME={name of TPU}
cd transformer/v2/
python3 transformer_main.py \
     --tpu=$TPU_NAME \
     --model_dir=$MODEL_DIR \
     --data_dir=$GCS_DATA_DIR \
     --vocab_file=$GCS_DATA_DIR/vocab.ende.32768 \
     --bleu_source=$GCS_DATA_DIR/output-source.txt \
     --bleu_ref=$GCS_DATA_DIR/output-target.txt \
     --batch_size=6144 \
     --train_steps=300000 \
     --static_batch=true \
     --use_ctl=true \
     --param_set=big \
     --padded_decode=true \
     --distribution_strategy=tpu
