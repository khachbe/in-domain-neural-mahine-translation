#!/bin/bash
export PYTHONPATH="$PYTHONPATH:/usr/share/models"
export STORAGE_BUCKET=gs://{name of google storage bucket}
export GCS_DATA_DIR=$STORAGE_BUCKET/data/ufal_out
export MODEL_DIR=$STORAGE_BUCKET/transformer/model_out
export TPU_NAME={name of TPU}
#cd /usr/share/models/official/transformer/v2/
python3 transformer_main.py \
     --tpu=$TPU_NAME \
     --model_dir=$MODEL_DIR \
     --data_dir=$GCS_DATA_DIR \
     --vocab_file=$GCS_DATA_DIR/vocab.ende.32768 \
     --batch_size=6144 \
     --train_steps=200000 \
     --steps_between_evals=2000 \
     --static_batch=true \
     --use_ctl=true \
     --param_set=big \
     --padded_decode=true \
     --distribution_strategy=tpu
